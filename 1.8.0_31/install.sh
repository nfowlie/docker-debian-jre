#!/bin/bash

URL=http://download.oracle.com/otn-pub/java/jdk/8u31-b13/jdk-8u31-linux-x64.tar.gz
FILENAME=jdk-8u31-linux-x64.tar.gz
JAVA_HOME=/usr/lib/jvm/jdk1.8.0_31
INSTALL_DIR=/usr/lib/jvm

if [ ! -f "${FILENAME}" ]; then
    echo "Downloading ${FILENAME}..."

    /usr/bin/wget --continue --progress=bar --no-check-certificate --tries 3 --dns-timeout 30 --connect-timeout=30 --wait 30 --waitretry 30 --retry-connrefused --header="Cookie: oraclelicense=accept-securebackup-cookie" --output-document="${FILENAME}" "${URL}"

    if [ $? -ne 0 ]; then
        echo 0
    fi

    if [ ! -f ${FILENAME} ]; then
        echo "FAILED: Unable to retrieve ${URL}."
        exit 0;
    fi
fi

echo "Installing to ${INSTALL_DIR}"

if [ -d "${JAVA_HOME}" ]; then
    rm -rf "${JAVA_HOME}"
fi

mkdir --parents "${JAVA_HOME}"
tar -xvf "${FILENAME}" -C "${INSTALL_DIR}"

update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.8.0_31/bin/java 1065
update-alternatives --install /usr/bin/javaws javaws /usr/lib/jvm/jdk1.8.0_31/bin/javaws 1065

echo "Removing temporary files..."
rm -rf "${FILENAME}"
