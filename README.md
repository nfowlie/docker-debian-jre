This repository contains a collection of Dockerfiles that can be used to build a Debian base image running various versions of the Java Runtime Environment. 

### Usage ###

* Clone the repository `git clone https://nfowlie@bitbucket.org/nfowlie/docker-debian-jre.git`.
* Execute `sudo docker build --rm=true --tag debian-jre:latest` to build the image.